import React, {Component} from "react";

class MiprimerComponente extends Component{

    /*comentario de varias lineas 
    */
   //comentario de una sola linea
   
   
   //Metodo 
    render(){
        return(
            <div>
                <h1> Mi Primer Componente </h1>
                <h2> Subtitulo del Componente </h2>
            </div>
        );
    }
}

export default MiprimerComponente;