import logo from './logo.svg';
import './App.css';

// importar componentes
import MiPrimerComponente from "./components/MiprimerComponente";

function App() {
  return (
    <div className="App">
      
      <header className="App-header">
      <MiPrimerComponente/>
        <img src={logo} className="App-logo" alt="logo" />
        <h4>Hola Mundo</h4>
        <h3>Hola Mundo</h3>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
